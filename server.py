from server_ini import HOST,PORT,WIKI_ADDRESS
import http.server
import json
import threading
import socketserver
import shlex
import os
import posixpath
import shutil
import re
import requests
import urllib
import urllib.parse as urlparse2
from urllib.parse import urlparse, unquote
from subprocess import Popen, PIPE
import types
import main
import string

AVAILABLE_SERVICES=[]

#http://192.168.0.107:2526/all/?file=%22http://humanstxt.org/humans.txt%22

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def get_filter(filter):
    if "?" in filter:
        filter=filter[:filter.find("?")]
    return filter.split("/")[1:]

class ServiceHandler(http.server.BaseHTTPRequestHandler):
    def get_exitcode_stdout_stderr(self,cmd):
        args = shlex.split(cmd)
        proc = Popen(args, stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        try:
            query = (urlparse(self.path).query)
            params = dict(qc.split("=") for qc in query.split("&"))
            os.remove('temp/' + str(threading.get_ident()) + "_temporary."+unquote(params['file']).split(".")[-1][:-1])
        except:
            pass

        return exitcode, out, err

    def prepare_microservice(self,fname="default_input.txt"):
        query = (urlparse(self.path).query)
        params = ''
        if self.path.split("/")[1] == 'favicon.ico':
            return -1

        if len(self.path.split('/')) <= 1 or (len(self.path.split('/')) == 2 and self.path.split('/')[1]==''):
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            print_services=[{"service_name":s[1],"service_documentation":s[2]} for s in AVAILABLE_SERVICES]
            self.wfile.write(json.JSONEncoder().encode(print_services).encode('utf-8'))
            return -1
        else:
            if query!='':
                try:
                    if query != '':
                        params=(urlparse2.parse_qs(urlparse2.urlparse(self.path).query))

                        for p in params:
                            params[p]=''.join(params[p])
                    website = self.path.split("/")[1]
                    service = self.path.split("/")[2].split('?')[0]
                except (ValueError,IndexError):
                    self.send_error(400, "That is not a valid url", "Stop messing around will you?")
                    return -1

            filters=list(set(get_filter(self.path)))
            if "all" in filters and params=='':
                self.send_response(200)
                self.send_header("Content-type", "application/xml")
                self.end_headers()
                self.wfile.write(main.main(open(fname,'r').read()).encode('utf-8'))
                return -1

            f=''
            for s in AVAILABLE_SERVICES:
                if 'all' in filters:
                    f=main.main
                    break
                if filters[-1].lower()==s[1].lower():
                    f=s[0]
                    break
            if f=='':
                self.send_error(400,"We don't currently support any service with that name","Stop messing around will you?")
                return -1
            filename=''
            if 'file' in params:
                params["file"] = unquote(params["file"])
                try:
                    url = params["file"].replace('"','')
                    filename = "temp/" + str(threading.get_ident()) + '_temporary.'+unquote(params['file']).split(".")[-1][:-1].strip()

                    if not filename.lower().endswith('.txt'):
                        self.send_response(200)
                        self.send_header("Content-type", "application/json")
                        self.end_headers()
                        self.wfile.write(json.JSONEncoder().encode({"error_code":469,"message":"Invalid file extension"}).encode('utf-8'))
                        return -1
                    open(filename,"w").close()
                    request = requests.get(url, stream=True)
                    if request.status_code == 200:
                        with open(filename, 'ab') as image:
                            for chunk in request:
                                image.write(chunk)
                            image.close()
                    else:
                        self.send_error(412,"Unable to take image from url !")
                except Exception as e:
                    filename=self.deal_post_data()
                    if filename==False:
                        return -1
                self.send_response(200)
                res=str(f(open(filename,'r').read()))
                if res.startswith("<?xml version="):
                    self.send_header("Content-type", "application/xml")
                else:
                    self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write(res.encode('utf-8'))
                os.remove(filename)
                return -1

            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(str(f(open(fname,'r').read())).encode('utf-8'))
            return -1

    def do_GET(self):
        execution_string=self.prepare_microservice()
        if execution_string == -1:
            return
        output=self.get_exitcode_stdout_stderr(execution_string)
        if output[0] == 0:
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(output[1])
        else:
            self.send_error(output[0],output[2],"The service experienced an error")

    def do_POST(self):
        execution_string = self.prepare_microservice()
        if execution_string == -1:
            return
        output=self.get_exitcode_stdout_stderr(execution_string)
        if output[0] == 0:
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(output[1])
        else:
            self.send_error(output[0],output[2],"The service experienced an error")


    def deal_post_data(self):
        remainbytes = int(self.headers['content-length'])
        line = self.rfile.readline()
        remainbytes -= len(line)

        line = self.rfile.readline()
        remainbytes -= len(line)
        fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', str(line))
        if not fn:
            self.send_error(404,"File name not found", "Stop messing around will you?")
            return False
        fn = 'temp/'+str(threading.get_ident())+'_temporary.'+fn[0].split('.')[-1]
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)
        try:
            out = open(fn, 'wb')
        except IOError:
            self.send_error(500, "Can't create file to write, do you have permission to write?","Stop messing around will you?")
            return False

        if line.strip():
            preline = line
        else:
            preline = self.rfile.readline()
        remainbytes -= len(preline)
        while remainbytes > 0:
            line = self.rfile.readline()
            remainbytes -= len(line)
            out.write(preline)
            preline = line
        return fn

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)

        """
        # abandon query parameters
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]
        path = posixpath.normpath(urllib.parse.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd()
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        return path

    def copyfile(self, source, outputfile):
        """Copy all data between two file objects.

        The SOURCE argument is a file object open for reading
        (or anything with a read() method) and the DESTINATION
        argument is a file object open for writing (or
        anything with a write() method).

        The only reason for overriding this would be to change
        the block size or perhaps to replace newlines by CRLF
        -- note however that this the default server uses this
        to copy binary data as well.

        """
        shutil.copyfileobj(source, outputfile)

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

if __name__ == "__main__":
    AVAILABLE_SERVICES=([(main.__dict__.get(a),str(main.__dict__.get(a).__name__),main.__dict__.get(a).__doc__) for a in dir(main) if isinstance(main.__dict__.get(a), types.FunctionType) and not a.startswith("_") and not a in ["Comment","tostring","word_tokenize", "sent_tokenize","main"]])

    server = ThreadedTCPServer((HOST, PORT), ServiceHandler)

    ip, port = server.server_address
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()
        pass
